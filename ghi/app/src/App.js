import { BrowserRouter, Routes, Route, Navigate } from 'react-router-dom';
import AttendConferenceForm from './AttendConferenceForm';
import ConferenceForm from './ConferenceForm';
import LocationForm from './LocationForm';
import AttendeesList from './AttendeesList';
import Nav from './Nav';
import PresentationForm from './PresentationForm';
import MainPage from './MainPage';

function App(props) {
  const defaultAttendees = props.attendees === undefined ? [] : props.attendees;

  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route path="/" element={<MainPage />} />
          <Route path="conferences/*">
            <Route path="new" element={<ConferenceForm />} />
          </Route>
          <Route path="attendees/*">
            <Route path="" element={<AttendeesList attendees={defaultAttendees} />} />
            <Route path="new" element={<AttendConferenceForm />} />
          </Route>
          <Route path="locations/*">
            <Route path="new" element={<LocationForm />} />
          </Route>
          <Route path="presentations/*">
            <Route path="new" element={<PresentationForm />} />
          </Route>
        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;
