import React, { useState, useEffect } from 'react';

function PresentationForm() {
  const [formValues, setFormValues] = useState({
    presenter_name: '',
    presenter_email: '',
    company_name: '',
    title: '',
    synopsis: '',
    conference: '',
  });
  const [conferences, setConferences] = useState([]);

  const handleChange = (event) => {
    setFormValues({
      ...formValues,
      [event.target.name]: event.target.value
    });
  }

  const handleSubmit = async (event) => {
    event.preventDefault();

    const conferenceId = formValues.conference;
    const locationUrl = `http://localhost:8000/api/conferences/${conferenceId}/presentations/`;
    const response = await fetch(locationUrl, {
      method: "POST",
      body: JSON.stringify(formValues),
      headers: {
        'Content-Type': 'application/json',
      },
    });

    if (response.ok) {
      setFormValues({
        presenter_name: '',
        presenter_email: '',
        company_name: '',
        title: '',
        synopsis: '',
        conference: '',
      });
      const newConference = await response.json();
      console.log(newConference);
    }
  }

  useEffect(() => {
    const fetchData = async () => {
      const url = 'http://localhost:8000/api/conferences/';
      const response = await fetch(url);

      if (response.ok) {
        const data = await response.json();
        setConferences(data.conferences);
      }
    }

    fetchData();
  }, []);

  return (
    <div className="container">
      <div className="row">
        <div className="offset-3 col-6">
          <div className="shadow p-4 mt-4">
            <h1>Create a new presentation</h1>
            <form id="create-presentation-form" onSubmit={handleSubmit}>
              <div className="form-floating mb-3">
                <input
                  placeholder="Presenter name"
                  required
                  type="text"
                  name="presenter_name"
                  id="presenter_name"
                  className="form-control"
                  value={formValues.presenter_name}
                  onChange={handleChange}
                />
                <label htmlFor="presenter_name">Presenter name</label>
              </div>
              <div className="form-floating mb-3">
                <input
                  placeholder="Presenter email"
                  required
                  type="email"
                  name="presenter_email"
                  id="presenter_email"
                  className="form-control"
                  value={formValues.presenter_email}
                  onChange={handleChange}
                />
                <label htmlFor="presenter_email">Presenter email</label>
              </div>
              <div className="form-floating mb-3">
                <input
                  placeholder="Company name"
                  type="text"
                  name="company_name"
                  id="company_name"
                  className="form-control"
                  value={formValues.company_name}
                  onChange={handleChange}
                />
                <label htmlFor="company_name">Company name</label>
              </div>
              <div className="form-floating mb-3">
                <input
                  placeholder="Title"
                  required
                  type="text"
                  name="title"
                  id="title"
                  className="form-control"
                  value={formValues.title}
                  onChange={handleChange}
                />
                <label htmlFor="title">Title</label>
              </div>
              <div className="mb-3">
                <label htmlFor="synopsis">Synopsis</label>
                <textarea
                    className="form-control"
                    id="synopsis"
                    rows="3"
                    name="synopsis"
                    value={formValues.synopsis}
                    onChange={handleChange}
                    />
              </div>
              <div className="mb-3">
                <select
                  required
                  name="conference"
                  id="conference"
                  className="form-select"
                  value={formValues.conference}
                  onChange={handleChange}
                >
                  <option value="">Choose a conference</option>
                  {conferences.map((conference) => (
                    <option key={conference.id} value={conference.id}>
                      {conference.name}
                    </option>
                  ))}
                </select>
              </div>
              <button className="btn btn-primary" type="submit">
                Create
              </button>
            </form>
          </div>
        </div>
      </div>
    </div>
  );
}

export default PresentationForm;
