import React, { useEffect, useState } from "react";

function ConferenceForm() {
  const [locations, setLocations] = useState([]);
  const [name, setName] = useState('');
  const [starts, setStarts] = useState('');
  const [ends, setEnds] = useState('');
  const [description, setDescription] = useState('');
  const [maxPresentations, setMaxPresentations] = useState('');
  const [maxAttendees, setMaxAttendees] = useState('');
  const [location, setLocation] = useState('');

  const fetchData = async () => {
    const url = "http://localhost:8000/api/locations/";
    const response = await fetch(url);
    if (response.ok) {
      const data = await response.json();
      setLocations(data.locations);
    }
  };

  const handleNameChange = (event) => {
    setName(event.target.value);
  };

  const handleStartsChange = (event) => {
    setStarts(event.target.value);
  };

  const handleEndsChange = (event) => {
    setEnds(event.target.value);
  };

  const handleDescriptionChange = (event) => {
    setDescription(event.target.value);
  };

  const handleMaxPresentationsChange = (event) => {
    setMaxPresentations(event.target.value);
  };

  const handleMaxAttendeesChange = (event) => {
    setMaxAttendees(event.target.value);
  };

  const handleLocationChange = (event) => {
    setLocation(event.target.value);
  };

  const handleSubmit = async (event) => {
    event.preventDefault();

    const data = {}
      data.name = name;
      data.starts = starts;
      data.ends = ends;
      data.description = description;
      data.max_presentations = maxPresentations;
      data.max_attendees = maxAttendees;
      data.location = location;


    const conferenceUrl = 'http://localhost:8000/api/conferences/';
    const fetchConfig = {
      method: 'POST',
      body: JSON.stringify(data),
      headers: {
        'Content-Type': 'application/json',
      },
    };

    try {
      const response = await fetch(conferenceUrl, fetchConfig);
      if (response.ok) {
        setName('');
        setStarts('');
        setEnds('');
        setDescription('');
        setMaxPresentations('');
        setMaxAttendees('');
        setLocation('');
        const newConference = await response.json();
        console.log(newConference);
      } else {
        console.error('Failed to create conference:', response.statusText);
      }
    } catch (error) {
      console.error('An error occurred while submitting the form:', error);
    }
  };

  useEffect(() => {
    fetchData();
  }, []);

  return (
    <div className="my-5 container">
      <div className="offset-3 col-6">
        <div className="shadow p-4 mt-4">
          <h1>Create a New Conference</h1>
          <form id="create-conference-form" onSubmit={handleSubmit}>
            <div className="form-floating mb-3">
              <input
              type="text"
              className="form-control"
              name="name"
              id="name"
              required value={name}
              onChange={handleNameChange}
              placeholder="Name"/>
              <label htmlFor="name">Name</label>
            </div>
            <div className="form-floating mb-3">
              <input type="date" className="form-control" name="starts" id="starts" required value={starts} onChange={handleStartsChange} placeholder="Starts"/>
              <label htmlFor="starts">Start Date</label>
            </div>
            <div className="form-floating mb-3">
              <input type="date" className="form-control" name="ends" id="ends" required value={ends} onChange={handleEndsChange} placeholder="Ends"/>
              <label htmlFor="ends">End Date</label>
            </div>
            <div className="form-floating mb-3">
              <textarea className="form-control" name="description" id="description" required value={description} onChange={handleDescriptionChange} placeholder="Description"></textarea>
              <label htmlFor="description">Description</label>
            </div>
            <div className="form-floating mb-3">
              <input type="number" className="form-control" name="max_presentations" id="max-presentations" required value={maxPresentations} onChange={handleMaxPresentationsChange} placeholder="Max presentations"/>
              <label htmlFor="max-presentations">Maximum Presentations</label>
            </div>
            <div className="form-floating mb-3">
              <input type="number" className="form-control" name="max_attendees" id="max-attendees" required value={maxAttendees} onChange={handleMaxAttendeesChange} placeholder="Max attendees"/>
              <label htmlFor="max-attendees">Maximum Attendees</label>
            </div>
            <div className="mb-3">
              <label htmlFor="location" className="form-label">
                Choose a location. Don't see your location?
                <a href="/new-location.html">Create a new location.</a>
              </label>
              <select name="location" required id="location" className="form-select" value={location} onChange={handleLocationChange}>
                <option value="">Locations</option>
                {locations.map((location) => {
                  return (
                    <option key={location.id} value={location.id}>
                      {location.name}
                    </option>
                  );
                })}
              </select>
            </div>
            <button type="submit" className="btn btn-primary">Create</button>
          </form>
        </div>
      </div>
    </div>
  );
}

  export default ConferenceForm;
