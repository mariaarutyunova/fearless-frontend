window.addEventListener('DOMContentLoaded', async () => {
    const selectTag = document.getElementById('conference');
    const loadingIcon = document.getElementById('loading-conference-spinner');

    const url = 'http://localhost:8000/api/conferences/';
    const response = await fetch(url);
    if (response.ok) {
      const data = await response.json();

      for (let conference of data.conferences) {
        const option = document.createElement('option');
        option.value = conference.href;
        option.innerHTML = conference.name;
        selectTag.appendChild(option);
      }

      loadingIcon.classList.add('d-none');
      selectTag.classList.remove('d-none');


    }
  });
  const formTag = document.getElementById('create-attendee-form');
  formTag.addEventListener('submit', async event => {
    event.preventDefault();
    console.log('need to submit the form data');
    const formData = new FormData(formTag);
    // formData.append('attendee', document.getElementById('attendee'));
    const json = JSON.stringify(Object.fromEntries(formData));
    console.log(json);

    const attendeeUrl = 'http://localhost:8001/api/attendees/';
    const fetchConfig = {
      method: 'POST',
      body: json,
      headers: {
        'Content-Type': 'application/json',
      },
    };

    try {
      const response = await fetch(attendeeUrl, fetchConfig);
      if (response.ok) {
        const formTag2 = document.getElementById("create-attendee-form")
        formTag2.classList.add('d-none')
        const divTag = document.getElementById('success-message')
        divTag.classList.remove('d-none')
        const newAttendee = await response.json()
        console.log(newAttendee)

      } else {
        console.error('Failed to create attendee:', response.statusText);

      }
    } catch (error) {
      console.error('An error occurred while submitting the form:', error);

    }
  });
