
window.addEventListener('DOMContentLoaded', async () => {
    const url = 'http://localhost:8000/api/locations/';
    const locationsResponse = await fetch(url);
    if (locationsResponse.ok) {
        const data = await locationsResponse.json();

        const selectTag = document.querySelector('select');
        for (let location of data.locations) {
            const option = document.createElement('option');
            option.value = location.id;
            option.innerHTML = location.name;
            selectTag.appendChild(option);
        }
    }

});

const formTag = document.getElementById('create-conference-form');
formTag.addEventListener('submit', async event => {
  event.preventDefault();
  console.log('need to submit the form data');
  const formData = new FormData(formTag);
//   formData.append('conference', document.getElementById('conference'));
  const json = JSON.stringify(Object.fromEntries(formData));
  console.log(json);

  const conferenceUrl = 'http://localhost:8000/api/conferences/';
  const fetchConfig = {
    method: 'POST',
    body: json,
    headers: {
      'Content-Type': 'application/json',
    },
  };

  try {
    const response = await fetch(conferenceUrl, fetchConfig);
    if (response.ok) {
      formTag.reset();
      const newConference = await response.json();
      console.log(newConference);
    } else {
      console.error('Failed to create conference:', response.statusText);

    }
  } catch (error) {
    console.error('An error occurred while submitting the form:', error);

  }
});
